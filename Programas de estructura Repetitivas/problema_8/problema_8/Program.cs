﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace problema_8
{
    class Program
    {
        static void Main(string[] args)
        {
            char letra;

            for (letra = 'Z'; letra >= 'A'; letra--)
                Console.Write("{0} - ", letra);

            Console.ReadKey();
        }
    }
}
